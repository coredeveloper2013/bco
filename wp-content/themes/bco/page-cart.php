<?php get_header(); ?>

<?php if ( have_posts() ) : ?>

    <?php while ( have_posts() ) : the_post(); ?>

        <?php get_template_part( 'template-part', 'title' );?>

            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <p></p>
                        <?php the_content(); ?>
                        <p></p>
                    </div>
                </div>
            </div>

    <?php endwhile; ?>

<?php endif;?>

<?php get_footer();?>
