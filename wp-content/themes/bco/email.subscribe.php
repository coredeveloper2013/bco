<?php
include '../../../wp-load.php';

if( !empty($_POST['nonce']) && !empty($_POST['email']) ) :

    if( !wp_verify_nonce($_POST['nonce'], 'post_email') ):
        die('No script kiddies please.');
    endif;

    if( !filter_var($_POST['email'], FILTER_VALIDATE_EMAIL) ):
        die('Please, provide a valid email.');
    endif;

	$email = stripslashes(strip_tags($_POST['email']));

    global $wpdb;
    $table  = $wpdb->prefix.'es_emaillist';

    $my_results = $wpdb->get_row( "SELECT es_email_mail FROM $table WHERE es_email_mail = '$email'" );

    if( !empty($my_results) ):
        die($email.' already exists.');
    endif;

    $data   = array(
        'es_email_name'     => 'BCO Subscriber',
        'es_email_mail'     => $email,
        'es_email_status'   => 'Confirmed',
        'es_email_created'  => date('Y-m-d H:i:s'),
        'es_email_viewcount'=> 0,
        'es_email_group'    => 'Public',
        'es_email_guid'     => generate_guid(),
    );
    $format = array('%s','%s','%s','%s','%d','%s','%s');
    $wpdb->insert($table,$data,$format);
    $my_id = $wpdb->insert_id;

    if( !empty($my_id) ):
        die($email);
    endif;

endif;
die();
