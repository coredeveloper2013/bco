<?php get_header(); ?>

<?php get_template_part( 'template-part', 'breadcrum' );?>

<?php if ( have_posts() ) : ?>

    <?php while ( have_posts() ) : the_post(); ?>
        <?php $product = wc_get_product( get_the_id() ); ?>

<section class="single_product_area">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="single_product_description">
                    <div class="product_sort_desc">
                        <?php echo previous_post_link( '%link', '<i class="fa fa-caret-left" aria-hidden="true"></i>' ); ?>
                        <?php echo next_post_link( '%link', '<i class="fa fa-caret-right" aria-hidden="true"></i>' ); ?>
                        <h2><?php echo $product->get_title(); ?></h2>
                        <h3>
                            <?php if( (int)$product->get_average_rating() ): ?>
                            <a href="javascript:void(0)" class="product_rating">
                                <?php for( $i=1; $i <= 5; $i++ ): ?>
                                    <?php if( $i <= (int)$product->get_average_rating() ):?>
                                        <i class="fa fa-star-o yellow_star" aria-hidden="true"></i>
                                    <?php else:?>
                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                    <?php endif;?>
                                <?php endfor;?>
                            </a>
                            <?php else:?>
                            <a href="javascript:void(0)" class="product_rating">
                                <i class="fa fa-star-o" aria-hidden="true"></i>
                                <i class="fa fa-star-o" aria-hidden="true"></i>
                                <i class="fa fa-star-o" aria-hidden="true"></i>
                                <i class="fa fa-star-o" aria-hidden="true"></i>
                                <i class="fa fa-star-o" aria-hidden="true"></i>
                            </a>
                            <?php endif;?>
                            <span><?php echo $product->get_review_count(); ?> review (s) </span>
                        </h3>
                        <h2 class="c_price">
                            <span>
                                <?php
                                if($product->get_regular_price())
                                    echo get_woocommerce_currency_symbol().$product->get_regular_price() .' - ';
                                ?>
                            </span>
                            <?php
                                if($product->get_price())
                                    echo get_woocommerce_currency_symbol().$product->get_price();
                            ?>
                            <input type="hidden" id="product_price" value="<?php echo $product->get_price(); ?>">
                            <input type="hidden" id="product_id" value="<?php echo $product->get_id(); ?>">
                        </h2>
                        <h2 class="desc_title">Short description</h2>
                        <p><?php echo get_the_excerpt(); ?></p>
                    </div>

                    <?php if( get_field('composit_product') == 'yes' ): ?>

                        <div class="product_desc_details product_desc_details2">
                            <div class="single_cart">
                                <a href="#build-a-seat" id="scroll-hot-link" class="c_add_cart"> Build a Seat</a>
                            </div>
                        </div>

                    <?php else: ?>

                        <div class="product_desc_details">

                            <?php if( get_the_terms( get_the_id(), 'pa_color' ) ): ?>
                                <h3>Color</h3>
                                <div class="product_color">
                                <?php foreach( get_the_terms( get_the_id(), 'pa_color' )  as $term): ?>
                                    <style>.product_color_inner.product_color_<?php echo $term->slug;?> label::before { background: <?php echo $term->slug;?> !important; }</style>
                                    <div class="product_color_inner product_color_<?php echo $term->slug;?>">
                                        <input type="radio" id="color_<?php echo $term->slug;?>" name="product_color" value="<?php echo $term->slug;?>">
                                        <label for="color_<?php echo $term->slug;?>"></label>
                                    </div>
                                <?php endforeach; ?>
                                </div>
                            <?php endif; ?>

                            <?php if( get_the_terms( get_the_id(), 'pa_size' ) ): ?>
                                <h3>SIZE</h3>
                                <div class="product_size">
                                    <?php foreach( get_the_terms( get_the_id(), 'pa_size' )  as $term): ?>
                                        <div class="product_size_inner product_size_<?php echo $term->slug;?>">
                                          <input type="radio" id="size_<?php echo $term->slug;?>" name="product_size" value="<?php echo $term->slug;?>">
                                          <label for="size_<?php echo $term->slug;?>"><?php echo $term->name;?></label>
                                        </div>
                                    <?php endforeach; ?>
                                </div>
                            <?php endif;?>

                            <div class="single_cart">
                                <div class="num_count">
                                    <form>
                                          <p>
                                            <img src="<?php echo get_template_directory_uri();?>/images/minus.png" id="minus1" width="20" height="20" class="minus">
                                            <input id="product_quantity" type="text" value="1" class="qty">
                                            <img id="add1" src="<?php echo get_template_directory_uri();?>/images/plus.png" width="20" height="20" class="add">
                                        </p>
                                    </form>
                                </div>
                                <a href="javascript:void(0);" class="c_add_cart" onclick="BCOAddToCart();"><i class="fa fa-shopping-basket" aria-hidden="true"></i> Add to cart</a>
                                <a href="#"><i class="fa fa-search" aria-hidden="true"></i></a>
                                <a href="#"><i class="fa fa-heart" aria-hidden="true"></i></a>
                                <a href="#"><i class="fa fa-bitbucket" aria-hidden="true"></i></a>
                            </div>
                        </div>
                        <div class="single_category">
                            <?php if($product->get_sku): ?>
                                <h3>SKU: <span> <?php echo $product->get_sku(); ?></span></h3>
                            <?php endif;?>

                            <?php if( get_the_terms( get_the_id(), 'product_cat' ) ): ?>
                            <h3>Categories: <span>
                                <?php $terms = []; ?>
                                <?php foreach( get_the_terms( get_the_id(), 'product_cat' )  as $term): ?>
                                    <?php $terms[] = $term->name; ?>
                                <?php endforeach; ?>
                                <?php echo implode($terms, ', '); ?>
                                </span></h3>
                            <?php endif; ?>

                            <?php if( get_the_terms( get_the_id(), 'product_tag' ) ): ?>
                            <h3>Tags: <span>
                                <?php $terms = []; ?>
                                <?php foreach( get_the_terms( get_the_id(), 'product_tag' )  as $term): ?>
                                    <?php $terms[] = $term->name; ?>
                                <?php endforeach; ?>
                                <?php echo implode($terms, ', '); ?>
                            </span></h3>
                        <?php endif; ?>
                        </div>

                    <?php endif; ?>

                </div>
            </div>
            <div class="col-md-6">

                <?php $attachment_ids = $product->get_gallery_attachment_ids(); ?>

                <?php if( !empty($attachment_ids) ): ?>
                <div class="single_product_slider">
                        <div class="slider1 flexslider">
                            <ul class="slides">
                                <?php foreach( $attachment_ids as $attachment_id ): $image_link = wp_get_attachment_url( $attachment_id ); ?>
                                <li>
                                    <img src="<?php echo $image_link; ?>" />
                                     <a class="image-link" href="<?php echo $image_link; ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/expand.png" alt=""></a>
                                </li>
                                <?php endforeach;?>
                            </ul>
                        </div>
                        <div  class="carousel1 carousel flexslider">
                            <ul class="slides">
                                <?php foreach( $attachment_ids as $attachment_id ): $image_link = wp_get_attachment_url( $attachment_id ); ?>
                                <li>
                                    <img width="160" height="90" src="<?php echo $image_link; ?>" />
                                </li>
                                <?php endforeach;?>
                            </ul>
                        </div>
                </div>
                <?php endif;?>

                <div class="single_social">
                    <span></span>
                    <?php echo wpfai_social(); ?>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- =========================
    END BREDCRUMBS SECTION
============================== -->

<!-- =========================
            START ORDER HISTORY SECTION
        ============================== -->
        <section class="single_product_tab_area">
            <div class="single_product_tab_title">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <ul class="nav nav-tabs" id="myTab" role="tablist">
                              <li class="nav-item">
                                <a class="nav-link <?php echo empty($_GET['replytocom']) ? 'active show' : '';?>" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Description</a>
                              </li>
                              <li class="nav-item">
                                <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">ADDITIONAL INFORMATION</a>
                              </li>
                              <li class="nav-item <?php echo !empty($_GET['replytocom']) ? 'active show' : '';?>">
                                <a class="nav-link " id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">REVIEWS (<?php echo $product->get_review_count(); ?>)</a>
                              </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="single_product_tab_content">
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade <?php echo empty($_GET['replytocom']) ? 'show active' : '';?>" id="home" role="tabpanel" aria-labelledby="home-tab">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-12">
                                        <?php echo $product->description; ?>
                                    </div>
                                </div>
                            </div>
                      </div>
                    <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                          <div class="container">
                              <div class="row">
                                  <div class="col-md-12">
                                      <table class="table table-stripped">
                                            <tbody>
                                                <tr>
                                                    <th>Weight</th>
                                                    <td class="product_weight"><?php echo $product->get_weight().' '.get_option('woocommerce_weight_unit'); ?></td>
                                                </tr>
                                                <tr>
                                                    <th>Dimensions</th>
                                                    <td class="product_dimensions"><?php echo $product->get_length().' x '.$product->get_width().' x '.$product->get_height().' '.get_option('woocommerce_dimension_unit'); ?></td>
                                                </tr>
                                                <tr>
                                                    <th>Color</th>
                                                    <td>
                                                        <p><?php $terms = []; ?>
                                                        <?php if( get_the_terms( get_the_id(), 'pa_color' ) ): ?>
                                                        <?php foreach( get_the_terms( get_the_id(), 'pa_color' )  as $term): ?>
                                                            <?php $terms[] = $term->name; ?>
                                                        <?php endforeach; ?>
                                                        <?php echo implode($terms, ', '); ?>
                                                        <?php endif; ?></p>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th>Size</th>
                                                    <td>
                                                        <p><?php $terms = []; ?>
                                                        <?php if( get_the_terms( get_the_id(), 'pa_size' ) ): ?>
                                                        <?php foreach( get_the_terms( get_the_id(), 'pa_size' )  as $term): ?>
                                                            <?php $terms[] = $term->name; ?>
                                                        <?php endforeach; ?>
                                                        <?php echo implode($terms, ', '); ?>
                                                        <?php endif; ?></p>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                  </div>
                              </div>
                          </div>
                      </div>
                    <div class="tab-pane fade <?php echo !empty($_GET['replytocom']) ? 'show active' : '';?>" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                          <div class="container">
                              <div class="row">
                                  <div class="col-md-12">
                                      <?php echo do_shortcode( '[cusrev_reviews]' ) ;?>
                                  </div>
                              </div>
                          </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- =========================
            END ORDER HISTORY SECTION
        ============================== -->
<?php endwhile; ?>

<?php endif;?>

<?php
if( get_field('composit_product') == 'yes' ):
    get_template_part( 'template-part', 'buggy' );
else:
    get_template_part( 'template-part', 'related' );
endif;
?>

<?php get_template_part( 'template-part', 'instagram' ); ?>
<?php get_template_part( 'template-part', 'newsletter' );?>

<?php get_footer(); ?>

<script>
    function BCOAddToCart() {
        var product     = jQuery('#product_id').val();
        var price       = jQuery('#product_price').val();
        var quantity    = jQuery('#product_quantity').val();
        var color       = null;
        var size        = null;
        var cart_url    = '';
        var total       = null;
        var cart_amount = null;
        var currency_sn = "<?php echo get_woocommerce_currency_symbol(); ?>";

        jQuery('.c_add_cart').html('<i class="fa fa-spin fa-gear" aria-hidden="true"></i> Processing');

        if(product != '') {
            cart_url += "<?php echo home_url();?>/cart/?add-to-cart=" + product;
        }

        if( parseInt(quantity) < 1 ) {
            quantity = 1;
        }

        cart_url += "&quantity=" + quantity;

        jQuery("input[name='product_color']:checked").each(function() {
    		color = jQuery(this).val();
            cart_url += "&attribute_pa_colour=" + color;
    	});

        jQuery("input[name='product_size']:checked").each(function() {
    		size = jQuery(this).val();
            cart_url += "&attribute_pa_size=" + size;
    	});

        jQuery.ajax({url: cart_url, success: function(result) {

            if(typeof price == 'undefined' || price == null || price == '') {
                price = 0;
            }

            if(typeof quantity == 'undefined' || quantity == null || quantity == '') {
                quantity = 0;
            }

            total = parseFloat(price) * parseFloat(quantity);
            cart_amount = jQuery('.woocommerce-Price-amount.amount').text().replace(/[&\/\\#,+()$~%'":*?<>{}]/g, '');

            if(typeof cart_amount == 'undefined' || cart_amount == null || cart_amount == '' || isNaN(cart_amount)) {
                cart_amount = 0;
            }

            jQuery('.c_add_cart').html('<i class="fa fa-check" aria-hidden="true"></i> Added');
            jQuery('.cart-total-bco').html( ( parseInt(jQuery('.cart-total-bco').html()) + parseInt(quantity) ) );
            jQuery('.woocommerce-Price-amount.amount').html(  currency_sn + parseFloat( parseFloat(total) + parseFloat(cart_amount) ).toFixed(2) );

        }});

        return;
    }
</script>
