<?php
defined('ABSPATH') or die("Cannot access pages directly.");
function theme_customization_menu(){
	add_menu_page( 'Theme Manage', 'Theme Manage', 'manage_options', 'theme-customization', 'theme_customization_func',get_template_directory_uri().'/images/favicon.png', 152 );
}
add_action('admin_menu','theme_customization_menu');

function theme_customization_func(){

	if( isset($_POST['save-header-info']) ):

		update_option('header_text', stripslashes_deep($_POST['header_text']) );

		update_option('featured_product_title', $_POST['featured_product_title']);
		update_option('related_product_title', $_POST['related_product_title']);

		update_option('header_logo', $_POST['header_logo']);
		update_option('footer_logo', $_POST['footer_logo']);

		update_option('instagram_shortcode', stripslashes_deep( $_POST['instagram_shortcode'] ));
		update_option('option_instagram_title', $_POST['option_instagram_title']);

		update_option('facebook_link', $_POST['facebook_link']);
		update_option('twitter_link', $_POST['twitter_link']);
		update_option('instagram_link', $_POST['instagram_link']);
		update_option('pinterest_link', $_POST['pinterest_link']);
		update_option('google_plus_link', $_POST['google_plus_link']);
		update_option('youtube_link', $_POST['youtube_link']);

		update_option('option_newsletter_title', $_POST['option_newsletter_title']);
		update_option('option_newsletter_sub_title', $_POST['option_newsletter_sub_title']);
		update_option('option_newsletter_warning', $_POST['option_newsletter_warning']);

		update_option('footer_address', $_POST['footer_address_editor']);
		update_option('latest_blog_title', $_POST['latest_blog_title']);
		update_option('latest_product_title', $_POST['latest_product_title']);
		update_option('newsletter_title', $_POST['newsletter_title']);
		update_option('copyright_text', $_POST['copyright_text']);

		update_option('ad_blog', $_POST['ad_blog']);

		$success_message = '<p class="success-message" style="color:green; font-weight: bold;">Settings saved succefully.</p>';

	endif;

?>
<form action="" method="post">
<div style="width:60%;margin:0 auto;position:relative;margin-top:5%;left:5%;">
    <?php echo !empty($success_message) ? $success_message : null; ?>
    <h1>Theme Settings</h1>
<?php
if(function_exists( 'wp_enqueue_media' )){
    wp_enqueue_media();
}else{
    wp_enqueue_style('thickbox');
    wp_enqueue_script('media-upload');
    wp_enqueue_script('thickbox');
}
$default_logo = get_template_directory_uri().'/images/logo.png';
$default_logo_footer = get_template_directory_uri().'/images/footer-logo.png';
$default_ad_blog = get_template_directory_uri().'/images/addbanner.png';
?>
<p>
	<strong>Header Text:</strong>
	<br />
	<br />
	<textarea style="width: 60%; resize: none;" class="widfat" type="text" name="header_text"><?php echo get_option('header_text'); ?></textarea>
	<br>
</p>
<hr>
<p>
	<strong>Featured Product Text:</strong>
	<br />
	<br />
	<input class="header_text" type="text" name="featured_product_title" size="60" value="<?php echo get_option('featured_product_title'); ?>">
	<br>
</p>
<p>
	<strong>Related Product Text:</strong>
	<br />
	<br />
	<input class="header_text" type="text" name="related_product_title" size="60" value="<?php echo get_option('related_product_title'); ?>">
	<br>
</p>
<hr>
<p>
	<strong>Header Logo:</strong>
	<br />
	<br />
	<input class="header_logo_url" type="text" name="header_logo" size="60" value="<?php echo get_option('header_logo') ? get_option('header_logo') : $default_logo; ?>">
	<br>
	<img style="border:0;" class="header_logo" src="<?php echo get_option('header_logo') ? get_option('header_logo') : $default_logo; ?>"/>
	<br>
	<br>
	<button class="header_logo_upload button button-primary">Upload</button>
	<br>
</p>
<hr>
<p>
	<strong>Footer Logo:</strong>
	<br />
	<br />
	<input class="footer_logo_url" type="text" name="footer_logo" size="60" value="<?php echo get_option('footer_logo') ? get_option('footer_logo') : $default_logo_footer; ?>">
	<br>
	<img style="border:0;" class="footer_logo" src="<?php echo get_option('footer_logo') ? get_option('footer_logo') : $default_logo_footer; ?>"/>
	<br>
	<br>
	<button class="footer_logo_upload button button-primary">Upload</button>
	<br>
</p>
<hr>
<p>
	<strong>Footer Address:</strong>
	<?php
        $id = 'footer_address_editor';
		$content = get_option('footer_address');
		$content = $content ? $content : '
			10528-A Tanner Road <br>Houston, TX 77041 <br>
			Email: <a href="mailto:info@bigcountryoutdoors.net" target="_top">info@bigcountryoutdoors.net</a> <br>
			Phone: <span>713-461-9443</span>';
		wp_editor( stripslashes( $content ), $id );
	?>
</p>
<hr>
<p>
	<strong>Instagram Shortcode:</strong>
	<br>
	<textarea class="widfat" name="instagram_shortcode"><?php echo get_option('instagram_shortcode'); ?></textarea>
</p>
<hr>
<p>
	<strong>Instagram Title:</strong>
	<br>
	<input class="widfat" type="text" name="option_instagram_title" size="60" value="<?php echo get_option('option_instagram_title'); ?>">
</p>
<hr>
<p>
	<strong>Social Media Links:</strong>
	<br>
	<table>
		<tr>
			<td>
				<label>Facebook: </label>
			</td>
			<td>
				<input class="widfat" type="text" name="facebook_link" size="60" value="<?php echo get_option('facebook_link'); ?>">
			</td>
		</tr>
		<tr>
			<td>
				<label>Twitter: </label>
			</td>
			<td>
				<input class="widfat" type="text" name="twitter_link" size="60" value="<?php echo get_option('twitter_link'); ?>">
			</td>
		</tr>
		<tr>
			<td>
				<label>Instagram: </label>
			</td>
			<td>
				<input class="widfat" type="text" name="instagram_link" size="60" value="<?php echo get_option('instagram_link'); ?>">
			</td>
		</tr>
		<tr>
			<td>
				<label>Pinterest: </label>
			</td>
			<td>
				<input class="widfat" type="text" name="pinterest_link" size="60" value="<?php echo get_option('pinterest_link'); ?>">
			</td>
		</tr>
		<tr>
			<td>
				<label>Google Plus: </label>
			</td>
			<td>
				<input class="widfat" type="text" name="google_plus_link" size="60" value="<?php echo get_option('google_plus_link'); ?>">
			</td>
		</tr>
		<tr>
			<td>
				<label>Youtube: </label>
			</td>
			<td>
				<input class="widfat" type="text" name="youtube_link" size="60" value="<?php echo get_option('youtube_link'); ?>">
			</td>
		</tr>
	</table>
</p>
<hr>
<p>
	<strong>Footer Title:</strong>
	<br>
	<table>
		<tr>
			<td>
				<label>latest Blog Title: </label>
			</td>
			<td>
				<input class="widfat" type="text" name="latest_blog_title" size="60" value="<?php echo get_option('latest_blog_title'); ?>">
			</td>
		</tr>
		<tr>
			<td>
				<label>Latest Product Title: </label>
			</td>
			<td>
				<input class="widfat" type="text" name="latest_product_title" size="60" value="<?php echo get_option('latest_product_title'); ?>">
			</td>
		</tr>
		<tr>
			<td>
				<label>Newsletter Title: </label>
			</td>
			<td>
				<input class="widfat" type="text" name="newsletter_title" size="60" value="<?php echo get_option('newsletter_title'); ?>">
			</td>
		</tr>
		<tr>
			<td>
				<label>Copyright Text: </label>
			</td>
			<td>
				<input class="widfat" type="text" name="copyright_text" size="60" value="<?php echo get_option('copyright_text'); ?>">
			</td>
		</tr>
	</table>
</p>
<hr>
<p>
	<strong>Newsletter Section:</strong>
	<br>
	<table>
		<tr>
			<td>
				<label>Newsletter Title: </label>
			</td>
			<td>
				<input class="widfat" type="text" name="option_newsletter_title" size="60" value="<?php echo get_option('option_newsletter_title'); ?>">
			</td>
		</tr>
		<tr>
			<td>
				<label>Newsletter Sub Title: </label>
			</td>
			<td>
				<input class="widfat" type="text" name="option_newsletter_sub_title" size="60" value="<?php echo get_option('option_newsletter_sub_title'); ?>">
			</td>
		</tr>
		<tr>
			<td>
				<label>Newsletter Warning: </label>
			</td>
			<td>
				<input class="widfat" type="text" name="option_newsletter_warning" size="60" value="<?php echo get_option('option_newsletter_warning'); ?>">
			</td>
		</tr>
	</table>
</p>
<hr>
<p>
	<strong>Advertise Blog Page:</strong>
	<br />
	<br />
	<input class="ad_blog" type="text" name="ad_blog" size="60" value="<?php echo get_option('ad_blog') ? get_option('ad_blog') : $default_ad_blog; ?>">
	<br>
	<img style="border:0;" class="ad_blog_url" src="<?php echo get_option('ad_blog') ? get_option('ad_blog') : $default_ad_blog; ?>"/>
	<br>
	<br>
	<button class="ad_blog_upload button button-primary">Upload</button>
	<br>
</p>
<hr>
<p>
	<input name="save-header-info" type="submit" class="button button-primary" value="Save Information">
</p>
<?php echo !empty($success_message) ? $success_message : null; ?>
</div>
</form>
<script>
    jQuery(document).ready(function($) {

        setTimeout(function() {
            $('.success-message').fadeOut('fast');
        }, 1000);

    	if( ( $('.header_logo').attr('src') ) == '' ){
    		$('.header_logo').hide();
    	}

    	if( ( $('.footer_logo').attr('src') ) == '' ){
    		$('.footer_logo').hide();
    	}

    	if( ( $('.ad_blog_url').attr('src') ) == '' ){
    		$('.ad_blog_url').hide();
    	}

        $('.header_logo_upload').click(function(e) {
            e.preventDefault();

            var custom_uploader = wp.media({
                title: 'Custom Image',
                button: {
                    text: 'Upload Image'
                },
                multiple: false  // Set this to true to allow multiple files to be selected
            })
            .on('select', function() {
                var attachment = custom_uploader.state().get('selection').first().toJSON();
                $('.header_logo').attr('src', attachment.url);
                $('.header_logo_url').val(attachment.url);
                $('.header_logo').show();
            })
            .open();
        });

        $('.footer_logo_upload').click(function(e) {
            e.preventDefault();

            var custom_uploader = wp.media({
                title: 'Custom Image',
                button: {
                    text: 'Upload Image'
                },
                multiple: false  // Set this to true to allow multiple files to be selected
            })
            .on('select', function() {
                var attachment = custom_uploader.state().get('selection').first().toJSON();
                $('.footer_logo').attr('src', attachment.url);
                $('.footer_logo_url').val(attachment.url);
                $('.footer_logo').show();
            })
            .open();
        });

		$('.ad_blog_upload').click(function(e) {
            e.preventDefault();

            var custom_uploader = wp.media({
                title: 'Custom Image',
                button: {
                    text: 'Upload Image'
                },
                multiple: false  // Set this to true to allow multiple files to be selected
            })
            .on('select', function() {
                var attachment = custom_uploader.state().get('selection').first().toJSON();
                $('.ad_blog').attr('src', attachment.url);
                $('.ad_blog_url').val(attachment.url);
                $('.ad_blog').show();
            })
            .open();
        });
    });
</script>
<?php
}
?>
