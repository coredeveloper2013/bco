<section class="built_a_seat_area" id="build-a-seat">
    <div class="container buggy-timeline">
        <div class="row">
            <div class="col-md-12">
                <div class="buggy_content">
                    <ul>
                        <li>
                            <div class="buggy_content_inner active">
                                    <h2>choose your seat</h2>
                            </div>
                        </li>
                        <li>
                            <div class="buggy_content_inner">
                                <h2>Select Paint Color & Vinyl</h2>
                            </div>
                        </li>
                        <li>
                            <div class="buggy_content_inner">
                                <h2>pick baskets & accessories</h2>
                            </div>
                        </li>
                        <li>
                            <div class="buggy_content_inner">
                                <h2>Finalize seat</h2>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <?php
                    $counter = 1;
                    $query = new WP_Query( array(
                        'post_type' => 'product',
                        'post_status' => 'publish',
                        'showposts' => 10,
                        'meta_query' => array(
                            'relation' => 'AND',
                            array(
                                'key'    => 'composit_product',
                                'value'    => 'yes',
                            ),
                            array(
                                'key'    => 'product_fragment',
                                'value'    => 'no',
                            )
                     )
                    ) );
                ?>
                <div class="row">
                    <div class="col-md-12">
                    <?php if( $query->have_posts() ): ?>
                        <div id="build_a_seat_slider" class="owl-carousel owl-theme">
                        <?php while( $query->have_posts() ): $query->the_post();?>
                            <?php $product = wc_get_product( $query->post->ID );?>
                            <!-- <div class="col-md-6"> -->
                                <div class="feature_item text-center">
                                    <div class="feature_item_img">
                                        <a href="<?php the_permalink(); ?>"><img class="img-fluid" width="370" height="210" src="<?php echo !empty(wp_get_attachment_image_src( get_post_thumbnail_id( $loop->post->ID ), 'home-blog-thumb' )[0]) ? wp_get_attachment_image_src( get_post_thumbnail_id( $loop->post->ID ), 'home-blog-thumb' )[0] : wc_placeholder_img_src();?>" alt="<?php echo $product->get_title(); ?>"></a>
                                        <?php
                                        if( $product->is_on_sale() ){
                                            echo '<span>SALE</span>';
                                        } else {
                                            echo '<span>'.strtoupper($product->get_stock_status()).'</span>';
                                        }
                                        ?>
                                    </div>
                                    <h2><?php the_title(); ?></h2>
                                    <h3>
                                        <?php
                                        if($product->get_sale_price())
                                            echo get_woocommerce_currency_symbol().$product->get_sale_price() .' - ';
                                        ?>
                                        <span>
                                        <?php
                                        if($product->get_regular_price())
                                            echo get_woocommerce_currency_symbol().$product->get_regular_price();
                                        ?>
                                        </span>
                                    </h3>
                                    <?php if( (int)$product->get_average_rating() ): ?>
                                    <a href="javascript:void(0)" class="product_rating">
                                        <?php for( $i=1; $i <= (int)$product->get_average_rating(); $i++ ): ?>
                                            <i class="fa fa-star-o" aria-hidden="true"></i>
                                        <?php endfor;?>
                                    </a>
                                    <?php else:?>
                                    <a href="javascript:void(0)" class="product_rating">
                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                    </a>
                                    <?php endif;?>
                                    <p><?php echo get_the_excerpt(); ?></p>
                                    <input type="hidden" id="seat_image_<?php echo $product->get_id();?>" value="<?php echo !empty(wp_get_attachment_image_src( get_post_thumbnail_id( $loop->post->ID ), 'home-blog-thumb' )[0]) ? wp_get_attachment_image_src( get_post_thumbnail_id( $loop->post->ID ), 'home-blog-thumb' )[0] : wc_placeholder_img_src();?>">
                                    <a href="javascript:void(0)" class="added_seat seat_select_cart" onclick="chooseSeat(<?php echo $product->get_id();?>,<?php echo $product->get_price();?>,this)">Select</a>
                                </div>
                            <!-- </div> -->
                        <?php endwhile;?>
                        <?php wp_reset_query(); ?>
                    </div>
                    <?php endif; ?>
                    </div>
                </div>
                <div class="seat_select">
                    <div class="seat_select_inner clearfix">
                        <div class="seat_select_table">
                            <?php
                                $query = new WP_Query( array(
                                    'post_type' => 'product',
                                    'post_status' => 'publish',
                                    'showposts' => -1,
                                    'product_cat' => 'paint-color',
                                    'meta_query' => array(
                                		array(
                                			'taxonomy' => 'product_cat',
                                			'field'    => 'fragment_category',
                                			'value'    => 'yes',
                                		),
                                	),
                                ) );
                            ?>
                            <?php if( $query->have_posts() ): ?>
                            <h2>PAINT COLOR</h2>
                            <div class="table-responsive">
                                <table class="table">
                                    <tbody>
                                        <?php while( $query->have_posts() ): $query->the_post();?>
                                            <?php $product = wc_get_product( $query->post->ID );?>
                                            <tr>
                                                <td>
                                                    <div class="seat_radio">
                                                      <input type="radio" id="card<?php echo $counter;?>" name="paint_color" value="<?php echo $product->get_id(); ?>" onclick="choosePaint(<?php echo $product->get_id();?>,<?php echo $product->get_price();?>,this)">
                                                      <label for="card<?php echo $counter;?>"> <span id="paint_color_css_<?php echo $product->get_id(); ?>" class="s_color s_color_<?php echo $counter;?>" style="background: <?php echo get_field('fragment_color_code');?>;"></span><span id="color_name_<?php echo $product->get_id();?>"><?php the_title(); ?></span></label>
                                                    </div>
                                                </td>
                                                <td><?php echo get_woocommerce_currency_symbol().$product->get_price(); ?></td>
                                            </tr>
                                        <?php wp_reset_query(); ?>
                                        <?php $counter++; endwhile;?>
                                    </tbody>
                                </table>
                            </div>
                            <?php endif; ?>
                        </div>
                        <div class="seat_select_table seat_select_table_right">
                            <?php
                                $query = new WP_Query( array(
                                    'post_type' => 'product',
                                    'post_status' => 'publish',
                                    'showposts' => -1,
                                    'product_cat' => 'vinyl',
                                    'meta_query' => array(
                                		array(
                                			'taxonomy' => 'product_cat',
                                			'field'    => 'fragment_category',
                                			'value'    => 'yes',
                                		),
                                	),
                                ) );
                            ?>
                            <?php if( $query->have_posts() ): ?>
                            <h2>Vinyl</h2>
                            <div class="table-responsive">
                                <table class="table">
                                    <tbody>
                                        <?php while( $query->have_posts() ): $query->the_post();?>
                                            <?php $product = wc_get_product( $query->post->ID );?>
                                            <tr>
                                                <td>
                                                    <div class="seat_radio">
                                                      <input type="radio" id="card<?php echo $counter;?>" name="vinyl_color" value="<?php echo $product->get_id(); ?>" onclick="chooseVinyl(<?php echo $product->get_id();?>,<?php echo $product->get_price();?>,this)">
                                                      <label for="card<?php echo $counter;?>"> <span id="vinyl_color_css_<?php echo $product->get_id(); ?>" class="s_color s_color_<?php echo $counter;?>" style="background: <?php echo get_field('fragment_color_code');?>;"></span><span id="vinyl_name_<?php echo $product->get_id();?>"><?php the_title(); ?></span></label>
                                                    </div>
                                                </td>
                                                <td><?php echo get_woocommerce_currency_symbol().$product->get_price(); ?></td>
                                            </tr>
                                        <?php wp_reset_query(); ?>
                                        <?php $counter++; endwhile;?>
                                    </tbody>
                                </table>
                            </div>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="seat_select_inner clearfix">
                        <div class="seat_select_table">
                            <?php
                                $query = new WP_Query( array(
                                    'post_type' => 'product',
                                    'post_status' => 'publish',
                                    'showposts' => -1,
                                    'product_cat' => 'baskets',
                                    'meta_query' => array(
                                		array(
                                			'taxonomy' => 'product_cat',
                                			'field'    => 'fragment_category',
                                			'value'    => 'yes',
                                		),
                                	),
                                ) );
                            ?>
                            <?php if( $query->have_posts() ): ?>
                            <h2>Baskets</h2>
                            <div class="table-responsive">
                                <table class="table">
                                    <tbody>
                                        <?php while( $query->have_posts() ): $query->the_post();?>
                                            <?php $product = wc_get_product( $query->post->ID );?>
                                            <?php if('side-buckets' == $post->post_name): ?>
                                            <tr>
                                                <td>
                                                    <div class="seat_radio1">
                                                        <input type="radio" id="card21" name="basket_<?php echo $product->get_id();?>" onclick="chooseBasket(<?php echo $product->get_id();?>,<?php echo $product->get_price();?>,this)">
                                                        <label for="card21"><span id="basket_name_<?php echo $product->get_id();?>"><?php the_title(); ?></span></label>
                                                    </div>
                                                    <div class="num_count">
                                                        <form>
                                                            <p>
                                                                <img src="<?php echo get_template_directory_uri();?>/images/minus.png" id="minus1" width="20" height="20" class="minus">
                                                                <input id="basket_quantity_<?php echo $product->get_id();?>" type="text" value="1" class="qty">
                                                                <img id="add1" src="<?php echo get_template_directory_uri();?>/images/plus.png" width="20" height="20" class="add">
                                                            </p>
                                                        </form>
                                                    </div>
                                                </td>
                                                <td><?php echo get_woocommerce_currency_symbol().$product->get_price(); ?></td>
                                            </tr>
                                            <?php else:?>
                                            <tr>
                                                <td>
                                                    <div class="seat_radio1">
                                                        <input type="radio" id="card<?php echo $counter;?>" name="basket_<?php echo $product->get_id();?>" onclick="chooseBasket(<?php echo $product->get_id();?>,<?php echo $product->get_price();?>,this)">
                                                        <label for="card<?php echo $counter;?>"><span id="basket_name_<?php echo $product->get_id();?>"><?php the_title(); ?></span></label>
                                                    </div>
                                                </td>
                                                <td><?php echo get_woocommerce_currency_symbol().$product->get_price(); ?></td>
                                            </tr>
                                            <?php endif;?>
                                        <?php wp_reset_query(); ?>
                                        <?php $counter++; endwhile;?>
                                    </tbody>
                                </table>
                            </div>
                            <?php endif; ?>
                        </div>
                        <div class="seat_select_table seat_select_table_right">
                            <?php
                                $query = new WP_Query( array(
                                    'post_type' => 'product',
                                    'post_status' => 'publish',
                                    'showposts' => -1,
                                    'product_cat' => 'accessories',
                                    'meta_query' => array(
                                		array(
                                			'taxonomy' => 'product_cat',
                                			'field'    => 'fragment_category',
                                			'value'    => 'yes',
                                		),
                                	),
                                ) );
                            ?>
                            <?php if( $query->have_posts() ): ?>
                            <h2>Accessories</h2>
                            <div class="table-responsive">
                                <table class="table">
                                    <tbody>
                                        <?php while( $query->have_posts() ): $query->the_post();?>
                                            <?php $product = wc_get_product( $query->post->ID );?>
                                            <tr>
                                                <td>
                                                    <div class="seat_radio1">
                                                      <input type="radio" id="card<?php echo $counter;?>" name="accessories_<?php echo $product->get_id();?>" onclick="chooseAccessories(<?php echo $product->get_id();?>,<?php echo $product->get_price();?>,this)">
                                                      <label for="card<?php echo $counter;?>"><span id="accessories_name_<?php echo $product->get_id();?>"><?php the_title(); ?></span></label>
                                                    </div>
                                                </td>
                                                <td><?php echo get_woocommerce_currency_symbol().$product->get_price(); ?></td>
                                            </tr>
                                        <?php wp_reset_query(); ?>
                                        <?php $counter++; endwhile;?>
                                    </tbody>
                                </table>
                            </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 seat_wrapper">
                <div class="seat_details">

                    <h2 class="seat_white"><span>1</span> seat <span id="seat_price_area">0.00</span><span>$</span></h2>
                    <img id="seat_image_area" src="" class="img-fluid">

                    <h2 class="seat_white"><span>2</span>paint color &amp; Vinyl <span id="color_price_area">0.00</span><span>$</span></h2>
                    <div class="seat_details_table">
                        <div class="table-responsive">
                            <table class="table">
                                    <tbody>
                                        <tr id="color_result_area" style="display:none">
                                            <td>
                                                <div class="seat_details_inner">
                                                  <p><span id="selected_color_code" style="background: transparent;" class="seat_color"></span> Paint Color: <span id="selected_color_name"></span></p>
                                                </div>
                                            </td>
                                            <td><span>$</span><span id="selected_color_price">0.00</span></td>
                                        </tr>
                                        <tr id="vinyl_result_area" style="display:none">
                                            <td>
                                                <div class="seat_details_inner">
                                                  <p><span id="selected_vinyl_code" style="background: transparent;" class="seat_color"></span> Vinyl: <span id="selected_vinyl_name"></span></p>
                                                </div>
                                            </td>
                                            <td><span>$</span><span id="selected_vinyl_price">0.00</span></td>
                                        </tr>
                                    </tbody>
                            </table>
                        </div>
                    </div>
                    <h2 class="seat_white"><span>3</span> Baskets & accessories <span id="basket_price_area">0.00</span><span>$</span></h2>
                    <div class="seat_details_table seat_details_table2">
                        <div class="table-responsive">
                            <table class="table" id="basket_accessories_table">
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                    <h1 class="seat_total">Total <span id="mini_cart_total_area">0.00</span><span>$</span></h1>
                    <a href="javascript:void(0)" class="seat_total_btn" id="add-build-seat-mini-cart" onclick="AddToCartSeat()"> finalize seat & add to cart</a>
                    <p></p>
                </div>
            </div>
        </div>
    </div>
</section>

<script>

var carts_data = new Array();
var currency_sn = "<?php echo get_woocommerce_currency_symbol(); ?>";

function update_minicart_total(){

    var seat_total      = parseFloat( jQuery('#seat_price_area').html() );
    var color_total     = parseFloat( jQuery('#color_price_area').html() );
    var basket_total    = parseFloat( jQuery('#basket_price_area').html() );

    jQuery('#mini_cart_total_area').html( (seat_total + color_total + basket_total).toFixed(2) );
}

function update_paint_total(){

    var color_sub  = parseFloat( jQuery('#selected_color_price').html() );
    var vinyl_sub  = parseFloat( jQuery('#selected_vinyl_price').html() );

    jQuery('#color_price_area').html( (color_sub + vinyl_sub).toFixed(2) );
}

function update_basket_total(){

    var subtotal_basket = new Array();
    jQuery( ".total_basket_items" ).each(function( index ) {
        subtotal_basket.push( parseFloat( jQuery( this ).html() ).toFixed(2) );
    });

    sum_basket = 0;

    for (var i = 0; i < subtotal_basket.length; i++) {
        sum_basket += parseFloat(subtotal_basket[i]);
    }

    jQuery("#basket_price_area").html( sum_basket.toFixed(2) );
}

function chooseSeat(product_id, product_price, element){

    $this = jQuery(element);

    // jQuery('.seat_white').removeClass('seat_black');
    jQuery('#seat_price_area').parent().addClass('seat_black');

    jQuery('a.seat_select_cart').removeClass('added_seat1');
    jQuery('a.seat_select_cart').html('select');
    jQuery('div.buggy_content li:nth-child(1)').addClass('active');

    jQuery('#seat_price_area').html( parseFloat(product_price).toFixed(2) );
    jQuery('#seat_image_area').attr( 'src', jQuery('#seat_image_'+product_id).val() );

    jQuery('#seat_image_area').attr( 'counter_cart_url', "<?php echo home_url();?>/cart/?add-to-cart=" + product_id );

    $this.html('added');
    $this.addClass('added_seat1');

    update_minicart_total();
}

function choosePaint(product_id, product_price, element){

    $this = jQuery(element);

    // jQuery('.seat_white').removeClass('seat_black');
    jQuery('#color_price_area').parent().addClass('seat_black');

    jQuery('div.buggy_content li:nth-child(2)').addClass('active');
    jQuery('#color_result_area').show();
    jQuery('#color_price_area').html( parseFloat(product_price).toFixed(2) );
    jQuery('#selected_color_name').html( jQuery('#color_name_'+product_id).html() );
    jQuery('#selected_color_code').attr( 'style', jQuery('#paint_color_css_'+product_id).attr( 'style' ) );
    jQuery('#selected_color_price').html( parseFloat(product_price).toFixed(2) );

    jQuery('#selected_color_price').attr( 'counter_cart_url', "<?php echo home_url();?>/cart/?add-to-cart=" + product_id );

    update_paint_total();
    update_minicart_total();
}

function chooseVinyl(product_id, product_price, element){

    $this = jQuery(element);

    // jQuery('.seat_white').removeClass('seat_black');
    jQuery('#color_price_area').parent().addClass('seat_black');

    jQuery('div.buggy_content li:nth-child(2)').addClass('active');
    jQuery('#vinyl_result_area').show();
    jQuery('#vinyl_price_area').html( parseFloat(product_price).toFixed(2) );
    jQuery('#selected_vinyl_name').html( jQuery('#vinyl_name_'+product_id).html() );
    jQuery('#selected_vinyl_code').attr( 'style', jQuery('#vinyl_color_css_'+product_id).attr( 'style' ) );
    jQuery('#selected_vinyl_price').html( parseFloat(product_price).toFixed(2) );

    jQuery('#selected_vinyl_price').attr( 'counter_cart_url', "<?php echo home_url();?>/cart/?add-to-cart=" + product_id );

    update_paint_total();
    update_minicart_total();
}

function chooseBasket(product_id, product_price, element){

    $this = jQuery(element);

    var quantity = parseInt(jQuery('#basket_quantity_'+product_id).val());

    if(typeof quantity == 'undefined' || quantity == null || quantity == '' || isNaN(quantity)) {
        quantity = 1;
    }

    // jQuery('.seat_white').removeClass('seat_black');
    jQuery('#basket_price_area').parent().addClass('seat_black');

    jQuery('div.buggy_content li:nth-child(3)').addClass('active');
    jQuery('#basket_result_area').show();
    jQuery('#basket_price_area').html( parseFloat(product_price * quantity).toFixed(2) );

    if( document.getElementById("basket_"  +  product_id) !== null ) {
        jQuery('tr#basket_' + product_id).remove();
    }

    jQuery('#basket_accessories_table tbody').append('<tr id=basket_'+product_id+'><td><div class="seat_details_inner"><p><span class="seat_color"></span> <span>'+jQuery('#basket_name_'+product_id).html() + ' ('+quantity+'X)'+'</span></p></div></td><td><span>'+currency_sn+'</span><span class="total_basket_items">'+parseFloat(product_price * quantity).toFixed(2)+'</span></td></tr>')

    update_basket_total();
    update_minicart_total();

    jQuery('tr#basket_' + product_id).attr( 'counter_cart_url', "<?php echo home_url();?>/cart/?add-to-cart=" + product_id + '&quantity=' + quantity );
}

function chooseAccessories(product_id, product_price, element){

    $this = jQuery(element);

    // jQuery('.seat_white').removeClass('seat_black');
    jQuery('#basket_price_area').parent().addClass('seat_black');

    jQuery('div.buggy_content li:nth-child(4)').addClass('active');
    jQuery('#accessories_result_area').show();
    jQuery('#accessories_price_area').html( parseFloat(product_price).toFixed(2) );

    if( document.getElementById("accessories_"  +  product_id) !== null ) {
        jQuery('tr#accessories_' + product_id).remove();
    }

    var quantity = 1;

    jQuery('#basket_accessories_table tbody').append('<tr id=accessories_'+product_id+'><td><div class="seat_details_inner"><p><span class="seat_color"></span> <span>'+jQuery('#accessories_name_'+product_id).html() + '</span></p></div></td><td><span>'+currency_sn+'</span><span class="total_basket_items">'+parseFloat(product_price * quantity).toFixed(2)+'</span></td></tr>')

    update_basket_total();
    update_minicart_total();

    jQuery('tr#accessories_' + product_id).attr( 'counter_cart_url', "<?php echo home_url();?>/cart/?add-to-cart=" + product_id );
}

function AddToCartSeat(){

    var carts_data = new Array();

    jQuery('img[counter_cart_url]').each(function(){
        carts_data.push( jQuery(this).attr('counter_cart_url') );
    });

    jQuery('span[counter_cart_url]').each(function(){
        carts_data.push( jQuery(this).attr('counter_cart_url') );
    });

    jQuery('tr[counter_cart_url]').each(function(){
        carts_data.push( jQuery(this).attr('counter_cart_url') );
    });

    if (typeof carts_data[0] == 'undefined' || carts_data[0] == null || 0 < carts_data.length ){

        jQuery('#add-build-seat-mini-cart').addClass('disable-links');
        jQuery('#add-build-seat-mini-cart').html('<i class="fa fa-spin fa-gear" aria-hidden="true"></i> Processing.');

        var cart_urls = new Array();

        for(var key in carts_data) {
            cart_urls.push(carts_data[key]);
        }


        var timer_ticker = setInterval( function() {

            jQuery.ajax({url: cart_urls.pop(), async:false, success: function(result) {

            }});

            if(cart_urls === undefined || cart_urls.length == 0) {
                clearInterval(timer_ticker);
                window.location.href="<?php echo home_url('/cart'); ?>";
            }

        }, 2000 );

    } else{

        alert('Currently you do not have any product in cart. Please build a seat first.');
    }

}

jQuery(document).ready(function() {

    var seat        = jQuery(".seat_wrapper");
    var buggy       = jQuery(".buggy-timeline");
    var limit       = jQuery(".instagram_area");

    var limit_pos   = limit.position();
    var seat_pos    = seat.position();
    var buggy_pos   = buggy.position();

    jQuery(window).scroll(function() {

        var windowpos = jQuery(window).scrollTop();

        if (windowpos + 500 >= buggy_pos.top) {
            buggy.addClass("buggy_stick");
            seat.addClass("seat_stick");
        } else {
            buggy.removeClass("buggy_stick");
            seat.removeClass("seat_stick");
        }

        if (windowpos + 1900 >= limit_pos.top) {
            buggy.removeClass("buggy_stick");
            seat.removeClass("seat_stick");
        }

    });

});


</script>
