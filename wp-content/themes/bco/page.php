<?php get_header(); ?>

<?php if ( have_posts() ) : ?>

    <?php while ( have_posts() ) : the_post(); ?>

        <?php get_template_part( 'template-part', 'title' );?>

        <section class="blog_post_area">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 no-padding-left">
                        <p></p>
                        <?php the_content(); ?>
                        <p></p>
                    </div>
                </div>
            </div>
        </section>

    <?php endwhile; ?>

<?php endif;?>


<?php get_template_part( 'template-part', 'instagram' );?>
<?php get_template_part( 'template-part', 'newsletter' );?>

<?php get_footer();?>
