<?php
include '../../../wp-load.php';

if( !empty($_POST['cat_slug']) ) :

    $query = new WP_Query( array(
        'post_type' => 'product',
        'post_status' => 'publish',
        'showposts' => 10,
        'product_cat' => stripslashes(strip_tags($_POST['cat_slug'])),
        'meta_query' => array(
            array(
                'key'    => 'product_fragment',
                'value'    => 'no',
            )
        )
    ) );

    $html = '';

    if( $query->have_posts() ):
        $html .= '<div id="category_product_slider" class="owl-carousel owl-theme">';
        while( $query->have_posts() ): $query->the_post();
            $product = wc_get_product( $query->post->ID );

            $image = !empty(wp_get_attachment_image_src( get_post_thumbnail_id( $query->post->ID ), 'home-blog-thumb' )[0]) ? wp_get_attachment_image_src( get_post_thumbnail_id( $query->post->ID ), 'home-blog-thumb' )[0] : wc_placeholder_img_src();
            $price = !empty($product->get_regular_price()) ? get_woocommerce_currency_symbol().$product->get_regular_price() : '';

            $html .= '<div class="feature_item text-center">
                        <div class="feature_item_img">
                            <a href="'.get_the_permalink().'"><img width="328" height="182" src="'.$image.'" alt="'.$product->get_title().'"></a>
                        </div>
                        <h2><a href="'.get_the_permalink().'">'.$product->get_title().'</a></h2>
                        <h3>'.$price.'</h3>';

                        if( (int)$product->get_average_rating() ):

                            $html .= '<a href="javascript:void(0)" class="product_rating">';
                                for( $i=1; $i <= 5; $i++ ):
                                    if( $i <= (int)$product->get_average_rating() ):
                                        $html .= '<i class="fa fa-star-o yellow_star" aria-hidden="true"></i>';
                                    else:
                                        $html .= '<i class="fa fa-star-o" aria-hidden="true"></i>';
                                    endif;
                                endfor;
                            $html .= '</a>';

                        else:

                            $html .= '<a href="javascript:void(0)" class="product_rating">
                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                    </a>';

                        endif;

                    $html .= '</div>';

        endwhile;
        wp_reset_query();
        $html .= '</div>';
    endif;

endif;
die($html);
