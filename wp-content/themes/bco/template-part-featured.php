<!-- =========================
    START FEATURE PRODUCT SECTION
============================== -->
<section class="feature_product_area wow fadeInLeft">
    <div class="container">
        <div class="col-md-12">
            <div class="row">
                <div class="main_title main_title_2 text-center">
                    <h2><?php echo get_option('featured_product_title'); ?></h2>
                </div>
            </div>
        </div>
    </div>
    <div class="feature_product_inner_area">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <?php
                        $query = new WP_Query( array(
                            'post_type' => 'product',
                            'post_status' => 'publish',
                            'showposts' => 10,
                            'meta_query' => array(
                                array(
                                    'key'    => 'product_fragment',
                                    'value'    => 'no',
                                )
                            ),
                            'tax_query' => array( array(
                                'taxonomy' => 'product_visibility',
                                'field'    => 'term_id',
                                'terms'    => 'featured',
                                'operator' => 'IN',
                            ) )
                        ) );
                    ?>
                    <?php if( $query->have_posts() ): ?>
                        <div id="feature_slider" class="owl-carousel owl-theme">
                            <?php while( $query->have_posts() ): $query->the_post();?>
                                <?php $product = wc_get_product( $query->post->ID );?>
                                    <div class="feature_item text-center">
                                        <div class="feature_item_img">
                                            <a href="<?php echo $product->get_permalink(); ?>">
                                                <img width="370" height="210" src="<?php echo !empty(wp_get_attachment_image_src( get_post_thumbnail_id( $query->post->ID ), 'home-blog-thumb' )[0]) ? wp_get_attachment_image_src( get_post_thumbnail_id( $query->post->ID ), 'home-blog-thumb' )[0] : wc_placeholder_img_src();?>" alt="<?php echo $product->get_title(); ?>">
                                            </a>
                                            <?php
                                            if( $product->is_on_sale() ){
                                                echo '<span>SALE</span>';
                                            } else {
                                                echo '<span>'.strtoupper($product->get_stock_status()).'</span>';
                                            }
                                            ?>
                                        </div>
                                        <h2>
                                            <a href="<?php echo $product->get_permalink(); ?>">
                                                <?php echo $product->get_title(); ?>
                                            </a>
                                        </h2>
                                        <h3><?php
                                            if($product->get_sale_price())
                                                echo get_woocommerce_currency_symbol().$product->get_sale_price() .' - ';
                                            ?>
                                            <span>
                                            <?php
                                            if($product->get_regular_price())
                                                echo get_woocommerce_currency_symbol().$product->get_regular_price();
                                            ?>
                                            </span>
                                        </h3>

                                        <?php if( (int)$product->get_average_rating() ): ?>
                                        <a href="javascript:void(0)" class="product_rating">
                                            <?php for( $i=1; $i <= (int)$product->get_average_rating(); $i++ ): ?>
                                                <i class="fa fa-star-o" aria-hidden="true"></i>
                                            <?php endfor;?>
                                        </a>
                                        <?php else:?>
                                        <a href="javascript:void(0)" class="product_rating">
                                            <i class="fa fa-star-o" aria-hidden="true"></i>
                                            <i class="fa fa-star-o" aria-hidden="true"></i>
                                            <i class="fa fa-star-o" aria-hidden="true"></i>
                                            <i class="fa fa-star-o" aria-hidden="true"></i>
                                            <i class="fa fa-star-o" aria-hidden="true"></i>
                                        </a>
                                        <?php endif;?>

                                        <p><?php echo get_the_excerpt(); ?></p>
                                        <a href="<?php echo $product->get_permalink(); ?>">view product</a>
                                    </div>
                            <?php endwhile; ?>
                            <?php wp_reset_query(); ?>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- =========================
    END FEATURE PRODUCT SECTION
============================== -->
