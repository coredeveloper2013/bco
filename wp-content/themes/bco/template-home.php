<?php /* Template Name: Home Page Template */ ?>
<?php get_header();?>
    <?php while ( have_posts() ) : the_post(); ?>
        <!-- =========================
            START BANNER SECTION
        ============================== -->
        <section class="banner_area">
                <div class="home_banner_item parallax-window" style="background-image: url('<?php echo get_field('header_banner');?>')">
                    <div class="banner_inner text-center wow fadeInLeft">
                        <h2><?php echo get_field('header_title');?></h2>
                        <?php echo get_field('header_link');?>
                    </div>
                </div>
            <div class="banner_layer"></div>
        </section>
        <!-- =========================
            END BANNER SECTION
        ============================== -->
        
        <!-- =========================
            START WELCOME SECTION
        ============================== -->
        <section class="welcome_area wow fadeInRight">
            <div class="container">
                <div class="row">
                    <div class="col-md">
                        <div class="welcome_inner text-center ">
                            <?php the_content(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- =========================
            END WELCOME SECTION
        ============================== -->

        <!-- =========================
            START BUILT A SEAT SECTION
        ============================== -->
        <section class="built_seat_area  parallax-window" style="background-image: url(<?php echo get_field('built_seat_area_background');?>)">
            <div class="container">
                <div class="col-md-12">
                    <div class="row">
                        <div class="main_title text-center">
                            <?php echo get_field('built_a_seat');?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container wow fadeInLeft">
                <div class="row">
                    <?php if(  have_rows('built_a_seat_steps') ): $counter = 1;?>
                        <?php while ( have_rows('built_a_seat_steps') ) : the_row();?>
                        <div class="col-md-4">
                            <div class="built_seat_inner <?php echo $counter == 2 ? 'built_seat_inner_second' : ''; ?><?php echo $counter == 3 ? 'built_seat_inner_last' : ''; ?>">
                                <h2><?php the_sub_field('title'); ?></h2>
                                <p><?php the_sub_field('description'); ?></p>
                                <img src="<?php the_sub_field('image'); ?>" alt="<?php the_sub_field('title'); ?>">
                            </div>
                        </div>
                        <?php $counter++; endwhile;?>
                    <?php endif;?>
                    <div class="col-md-12">
                        <div class="built_seat_btn">
                            <?php echo get_field('built_a_seat_link'); ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        
        <!-- =========================
            END BUILT A SEAT SECTION
        ============================== -->

        <?php get_template_part( 'template-part', 'featured' );?>

        <!-- =========================
            START CATEGORY PRODUCT SECTION
        ============================== -->
        <section class="category_product_area wow fadeInRight">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="category_title">
                            <?php get_template_part('template-part', 'brand'); ?>
                        </div>
                    </div>
                </div>
            </div>

            <div class="feature_product_inner_area ">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12" id="slider-wrapper">
                            <?php
                                $query = new WP_Query( array(
                                    'post_type' => 'product',
                                    'post_status' => 'publish',
                                    'showposts' => 10,
                                    'product_cat' => $highlight,
                                    'meta_query' => array(
                                        array(
                                            'key'    => 'product_fragment',
                                            'value'    => 'no',
                                        )
                                    )
                                ) );
                            ?>

                            <?php if( $query->have_posts() ): ?>
                            <div id="category_product_slider" class="owl-carousel owl-theme">
                                <?php while( $query->have_posts() ): $query->the_post();?>
                                    <?php $product = wc_get_product( $query->post->ID );?>
                                    <div class="feature_item text-center">
                                        <div class="feature_item_img">
                                            <a href="<?php echo get_the_permalink(); ?>"><img width="328" height="182" src="<?php echo !empty(wp_get_attachment_image_src( get_post_thumbnail_id( $query->post->ID ), 'home-blog-thumb' )[0]) ? wp_get_attachment_image_src( get_post_thumbnail_id( $query->post->ID ), 'home-blog-thumb' )[0] : wc_placeholder_img_src();?>" alt="<?php echo $product->get_title(); ?>"></a>
                                        </div>
                                        <h2><a href="<?php echo get_the_permalink(); ?>"><?php echo $product->get_title(); ?></a></h2>
                                        <h3>
                                        <?php
                                        if($product->get_regular_price())
                                            echo get_woocommerce_currency_symbol().$product->get_regular_price();
                                        ?>
                                        </h3>

                                        <?php if( (int)$product->get_average_rating() ): ?>
                                        <a href="javascript:void(0)" class="product_rating">
                                            <?php for( $i=1; $i <= 5; $i++ ): ?>
                                                <?php if( $i <= (int)$product->get_average_rating() ):?>
                                                    <i class="fa fa-star-o yellow_star" aria-hidden="true"></i>
                                                <?php else:?>
                                                    <i class="fa fa-star-o" aria-hidden="true"></i>
                                                <?php endif;?>
                                            <?php endfor;?>
                                        </a>
                                        <?php else:?>
                                        <a href="javascript:void(0)" class="product_rating">
                                            <i class="fa fa-star-o" aria-hidden="true"></i>
                                            <i class="fa fa-star-o" aria-hidden="true"></i>
                                            <i class="fa fa-star-o" aria-hidden="true"></i>
                                            <i class="fa fa-star-o" aria-hidden="true"></i>
                                            <i class="fa fa-star-o" aria-hidden="true"></i>
                                        </a>
                                        <?php endif;?>

                                    </div>
                                <?php endwhile;?>
                                <?php wp_reset_query(); ?>
                            </div>
                            <?php endif;?>

                        </div>
                    </div>
                </div>
            </div>

        </section>
        <!-- =========================
            END CATEGORY PRODUCT SECTION
        ============================== -->

        <!-- =========================
            START FEATURE PRODUCT SECTION
        ============================== -->
        <section class="feature_product_area feature_product_layer wow fadeInLeft">
            <div class="build_a_buggy_layer"></div>
            <div class="container">
                <div class="col-md-12">
                    <div class="row">
                        <div class="main_title main_title_2 text-center">
                            <h2><?php echo get_field('best_seller_title'); ?></h2>
                        </div>
                    </div>
                </div>
            </div>
            <div class="feature_product_inner_area ">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12">
                            <?php
                                $args = array(
                                    'post_type' => 'product',
                                    'meta_key' => 'total_sales',
                                    'orderby' => 'meta_value_num',
                                    'showposts' => 10,
                                    'meta_query' => array(
                                        array(
                                            'key'    => 'product_fragment',
                                            'value'    => 'no',
                                        )
                                    )
                                );
                                $loop = new WP_Query( $args );
                            ?>

                            <?php if( $loop->have_posts() ): ?>
                            <div id="best_product_slider" class="owl-carousel owl-theme">

                                <?php while ( $loop->have_posts() ) : $loop->the_post(); global $product; ?>
                                <div class="feature_item text-center">
                                    <div class="feature_item_img">
                                        <a href="<?php the_permalink(); ?>"><img width="370" height="210" src="<?php echo !empty(wp_get_attachment_image_src( get_post_thumbnail_id( $loop->post->ID ), 'home-blog-thumb' )[0]) ? wp_get_attachment_image_src( get_post_thumbnail_id( $loop->post->ID ), 'home-blog-thumb' )[0] : wc_placeholder_img_src();?>" alt="<?php echo $product->get_title(); ?>"></a>
                                        <?php
                                        if( $product->is_on_sale() ){
                                            echo '<span>SALE</span>';
                                        } else {
                                            echo '<span>'.strtoupper($product->get_stock_status()).'</span>';
                                        }
                                        ?>
                                    </div>
                                    <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                                    <h3>
                                        <?php
                                        if($product->get_sale_price())
                                            echo get_woocommerce_currency_symbol().$product->get_sale_price() .' - ';
                                        ?>
                                        <span>
                                        <?php
                                        if($product->get_regular_price())
                                            echo get_woocommerce_currency_symbol().$product->get_regular_price();
                                        ?>
                                        </span>
                                    </h3>

                                    <?php if( (int)$product->get_average_rating() ): ?>
                                    <a href="javascript:void(0)" class="product_rating">
                                        <?php for( $i=1; $i <= 5; $i++ ): ?>
                                            <?php if( $i <= (int)$product->get_average_rating() ):?>
                                                <i class="fa fa-star-o yellow_star" aria-hidden="true"></i>
                                            <?php else:?>
                                                <i class="fa fa-star-o" aria-hidden="true"></i>
                                            <?php endif;?>
                                        <?php endfor;?>
                                    </a>
                                    <?php else:?>
                                    <a href="javascript:void(0)" class="product_rating">
                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                    </a>
                                    <?php endif;?>

                                    <p><?php echo get_the_excerpt(); ?></p>
                                    <a href="<?php the_permalink(); ?>">view product</a>
                                </div>
                               <?php endwhile; wp_reset_query(); ?>

                            </div>
                            <?php endif;?>

                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- =========================
            END FEATURE PRODUCT SECTION
        ============================== -->

        <!-- =========================
            START BUILD A BUGGY SECTION
        ============================== -->
        <section class="build_a_buggy_area ">
            <div class="container wow fadeInRight">
                <div class="col-md-12">
                    <div class="row">
                        <div class="main_title text-center">
                            <h2><?php echo get_field('build_a_buggy_title');?></h2>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container wow fadeInRight">
                <div class="row">

                    <?php if( have_rows('build_a_buggy_steps') ): $counter = 1;?>
                    <div class="col-md-3">
                        <?php while ( have_rows('build_a_buggy_steps') && $counter <= 3 ) : the_row();?>
                        <div class="built_seat_inner">
                            <h2><?php the_sub_field('title'); ?></h2>
                            <p><?php the_sub_field('content'); ?></p>
                            <img src="<?php the_sub_field('image'); ?>" alt="<?php the_sub_field('title'); ?>">
                        </div>
                        <?php $counter++; endwhile;?>
                    </div>
                    <?php endif;?>

                    <div class="col-md-6">
                        <div class="build_buggy_img">
                            <p><?php echo get_field('build_a_buggy_content');?></p>
                            <img src="<?php echo get_field('build_a_buggy_image');?>"  class="img-fluid" alt="<?php echo get_field('build_a_buggy_title');?>">
                        </div>
                    </div>

                    <?php if( have_rows('build_a_buggy_steps') ): $counter = 1;?>
                    <div class="col-md-3">
                        <?php while ( have_rows('build_a_buggy_steps') && $counter <= 3 ) : the_row();?>
                        <div class="built_seat_inner">
                            <h2><?php the_sub_field('title'); ?></h2>
                            <p><?php the_sub_field('content'); ?></p>
                            <img src="<?php the_sub_field('image'); ?>" alt="<?php the_sub_field('title'); ?>">
                        </div>
                        <?php $counter++; endwhile;?>
                    </div>
                    <?php endif;?>

                    <div class="col-md-12">
                        <div class="build_a_buggy_btn">
                            <a href="<?php echo get_field('build_your_seat_link');?>"><?php echo get_field('build_your_seat_text');?></a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- =========================
            END BUILD A BUGGY SECTION
        ============================== -->


        <!-- =========================
            START BRAND SECTION
        ============================== -->
        <section class="brand_area">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="brand_inner wow fadeInRight">
                            <?php if(get_sponsors()):?>
                            <ul>
                                <?php foreach( get_sponsors() as $sponsor ): ?>
                                    <li><a href="#"><img src="<?php echo $sponsor->image;?>" alt=""></a></li>
                                <?php endforeach;?>
                            </ul>
                            <?php endif;?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- =========================
            END BRAND SECTION
        ============================== -->


        <!-- =========================
            START CUSTOM SECTION
        ============================== -->
        <section class="built_seat_area custom_work_area parallax-window" style="background-image: url(<?php echo get_field('custom_work_area_background');?>)">
            <div class="container">
                <div class="col-md-12">
                    <div class="row">
                        <div class="main_title text-center">
                            <?php echo get_field('service_description');?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container wow fadeInRight">
                <div class="row">
                    <?php if( have_rows('service_steps') ): $i = 1; ?>
                        <?php while ( have_rows('service_steps') ) : the_row(); $class = '';?>
                        <?php
                        if($i==2):
                            $class = 'built_seat_inner_second';
                        endif;
                        if($i==3):
                            $class = 'built_seat_inner_last';
                        endif;
                        ?>
                        <div class="col-md-4">
                            <div class="built_seat_inner <?php echo $class;?>">
                                <h2><?php the_sub_field('title'); ?></h2>
                                <p><?php the_sub_field('description'); ?></p>
                                <img src="<?php the_sub_field('image'); ?>" alt="<?php the_sub_field('title'); ?>">
                            </div>
                        </div>
                        <?php $i++; endwhile;?>
                    <?php endif;?>
                    <div class="col-md-12">
                        <div class="built_seat_btn">
                            <?php echo get_field('service_link'); ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- =========================
            END CUSTOM SECTION
        ============================== -->


        <!-- =========================
            START HOME BLOG SECTION
        ============================== -->
        <?php if(get_blogs()):?>
        <section class="home_blog_area wow fadeInRight">
            <div class="container">
                <div class="col-md-12">
                    <div class="row">
                        <div class="main_title main_title_2 text-center">
                            <h2><?php echo get_field('blog_title'); ?></h2>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <?php foreach(get_blogs() as $blog): ?>
                    <div class="col-md-4">
                        <div class="home_blog_inner">
                            <div class="home_blog_inner_img">
                                <a href="<?php echo $blog->link;?>"><img src="<?php echo $blog->image;?>" alt="<?php echo $blog->image;?>" class="img-fluid"></a>
                            </div>
                            <h2><a href="<?php echo $blog->link;?>"><?php echo $blog->title;?></h2></a>
                            <h3>By <span><?php echo $blog->author;?> </span></h3>
                            <p><?php echo $blog->excerpt;?></p>
                            <a href="<?php echo $blog->link;?>">read more</a>
                        </div>
                    </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </section>
        <?php endif;?>
        <!-- =========================
            END HOME BLOG SECTION
        ============================== -->

        <?php get_template_part( 'template-part', 'instagram' );?>
        <?php get_template_part( 'template-part', 'newsletter' );?>

    <?php endwhile;?>
<?php get_footer();?>
