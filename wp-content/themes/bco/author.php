<?php get_header(); ?>

<section class="blog_post_area">
    <div class="container">
        <div class="row">
            <div class="col-md-9">
                <div class="blog_post_left">

<!-- This sets the $curauth variable -->

    <?php
    $curauth = (isset($_GET['author_name'])) ? get_user_by('slug', $author_name) : get_userdata(intval($author));
    ?>

    <h2>About: <?php echo $curauth->nickname; ?></h2>
    <dl>
        <dt>Website</dt>
        <dd><a href="<?php echo $curauth->user_url; ?>"><?php echo $curauth->user_url; ?></a><br></dd>
        <dd>
            <dt>Avatar</dt>
            <img src="<?php echo get_avatar_url(get_the_author_meta('email'), array('size' => 70)); ?>" alt="<?php echo get_the_author();?>"><br>
        </dd>
        <dd>
            <dt>Profile</dt>
            <p><?php echo $curauth->user_description; ?></p><br>
        </dd>
    </dl>

    <h2>Posts by <?php echo $curauth->nickname; ?>:</h2>

    <ul class="blog_category">
<!-- The Loop -->

    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
        <li>
            <a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link: <?php the_title(); ?>">
            <p><?php the_title(); ?></a></p>
            <?php the_time('d M Y'); ?> in <?php the_category('&');?>
            <hr>
        </li>

    <?php endwhile; else: ?>
        <p><?php _e('No posts by this author.'); ?></p>

    <?php endif; ?>

<!-- End Loop -->

    </ul>
</div>
</div>
<?php get_sidebar(); ?>
</div>
</section>
<?php get_footer(); ?>
