<?php get_header(); ?>

<style>
body {
    background-color:white;
    background-attachment:scroll;
    background-repeat:no-repeat;
    background-position:center;
    background-size:cover;
    line-height:5px;
}
.wrapper {
    background-color:#e9ecef;
}
.display-1 {text-align:center;color:#f25a17;}
.display-1 .fa {animation:fa-spin 5s infinite linear;}
.display-3 {text-align:center;color:#f25a17;}
.lower-case {text-align:center;}
.jumbotron {margin-bottom: 0;}
</style>

<div class="wrapper">
    <div class="container-fluid" id="body-container-fluid">
        <div class="jumbotron">
            <h1 class="display-1">4<i class="fa  fa-spin fa-cog fa-3x"></i> 4</h1>
            <h1 class="display-3">ERROR</h1>
            <p class="lower-case">Aw !! webpage not found please try to refresh</p>
            <p class="lower-case">Maybe the page is under maintenance</p>
        </div>
    </div>
</div>

<?php get_footer(); ?>
