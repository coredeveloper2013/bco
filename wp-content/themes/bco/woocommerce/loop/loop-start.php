<?php
/**
 * Product Loop Start
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/loop/loop-start.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
?>

<?php
	$per_page = isset($_GET['limit']) ? $_GET['limit'] : 5;
	$order_by = isset($_GET['orderby']) ? $_GET['orderby'] : 'name';
	$paginate = isset($_GET['paginate']) ? $_GET['paginate'] : 1;

	$categories = get_query_var('product_cat');

	$args = array(
		'post_type'      => 'product',
		'post_status'    => 'publish',
		'posts_per_page' => $per_page,
		'orderby'        => $order_by,
		'paged'          => $paginate,
		'meta_query' => array(
			array(
				'key'    => 'product_fragment',
				'value'    => 'no',
			)
		)
	);

	if( !empty($categories) ):
		$args['product_cat'] = $categories;
	endif;

	$query = new WP_Query( $args );

	$total_posts = $query->found_posts;

	$offset      = $per_page * $paginate;
	$offset      = ($offset > $total_posts) ? $total_posts : $offset;

	$o_offset    = $per_page * ($paginate - 1);
	$o_offset    = empty($o_offset) ? 1 : ($o_offset + 1);
?>

<section class="category_filter_area">
	<div class="container">
		<div class="row">
			<div class="col-md-9">
				<div class="category_filter_inner">
					<form class="" action="" method="get">
						<button type="submit" class="cat_filter c_inner"><i class="fa fa-filter" aria-hidden="true"></i> Filter</button>
						<div class="category_view c_inner">
							<span class="th"><i class="fa fa-th" aria-hidden="true"></i></span>
							<span class="th_list active"><i class="fa fa-th-list" aria-hidden="true"></i></span>
						</div>
						<div class="cat_short_by c_inner">
							<span>Sort by:  </span>
							<select name="orderby">
								<option <?php echo $order_by == 'popularity' ? 'selected' : ''; ?> value="popularity">Sort by popularity</option>
								<option <?php echo $order_by == 'rating' ? 'selected' : ''; ?> value="rating">Sort by average rating</option>
								<option <?php echo $order_by == 'date' ? 'selected' : ''; ?> value="date">Sort by newness</option>
								<option <?php echo $order_by == 'price' ? 'selected' : ''; ?> value="price">Sort by price: low to high</option>
								<option <?php echo $order_by == 'price-desc' ? 'selected' : ''; ?> value="price-desc">Sort by price: high to low</option>
							</select>
						</div>
						<div class="cat_short_by c_inner">
							<span>Show:</span>
							<select name="limit">
							  <option <?php echo $per_page == 12 ? 'selected' : ''; ?> value="12">12</option>
							  <option <?php echo $per_page == 6 ? 'selected' : ''; ?> value="6">6</option>
							  <option <?php echo $per_page == 3 ? 'selected' : ''; ?> value="3">3</option>
							  <option <?php echo $per_page == 1 ? 'selected' : ''; ?> value="1">1</option>
							</select>
							<span>Per page</span>
						</div>
					</form>
				</div>
			</div>
			<div class="col-md-3">
				<div class="cat_buggy">
					<a href="<?php echo home_url('/build-a-buggy');?>">build a buggy</a>
				</div>
			</div>
		</div>
	</div>
	<br>

	<?php if( $query->have_posts() ): ?>
		<?php while( $query->have_posts() ): $query->the_post();?>
			<?php $product = wc_get_product( $query->post->ID );?>
			<div class="category_list_thumb_area">
				<div class="container category_list_thumb_inner">
					<div class="row">
						<div class="col-md-4">
							<div class="product_cat_img feature_item_img">
								<a href="<?php echo get_the_permalink(); ?>">
									<img style="width: 370px; height: 210px;" width="370" height="210" src="<?php echo !empty(wp_get_attachment_image_src( get_post_thumbnail_id( $loop->post->ID ), 'home-blog-thumb' )[0]) ? wp_get_attachment_image_src( get_post_thumbnail_id( $loop->post->ID ), 'home-blog-thumb' )[0] : wc_placeholder_img_src();?>" alt="<?php echo $product->get_title(); ?>">
								</a>
							</div>
						</div>
						<div class="col-md-5">
							<div class="product_cat_desc">
								<a href="<?php echo get_the_permalink(); ?>">
									<h2><?php the_title(); ?></h2>
								</a>
								<h3>
									<?php if( (int)$product->get_average_rating() ): ?>
									<a href="javascript:void(0)" class="product_rating">
										<?php for( $i=1; $i <= 5; $i++ ): ?>
											<?php if( $i <= (int)$product->get_average_rating() ):?>
												<i class="fa fa-star-o yellow_star" aria-hidden="true"></i>
											<?php else:?>
												<i class="fa fa-star-o" aria-hidden="true"></i>
											<?php endif;?>
										<?php endfor;?>
									</a>
									<?php else:?>
									<a href="javascript:void(0)" class="product_rating">
										<i class="fa fa-star-o" aria-hidden="true"></i>
										<i class="fa fa-star-o" aria-hidden="true"></i>
										<i class="fa fa-star-o" aria-hidden="true"></i>
										<i class="fa fa-star-o" aria-hidden="true"></i>
										<i class="fa fa-star-o" aria-hidden="true"></i>
									</a>
									<?php endif;?>
									<span><?php echo $product->get_review_count(); ?> review (s) </span>
								</h3>
								<p><?php echo get_the_excerpt(); ?></p>
							</div>
						</div>
						<div class="col-md-3">
							<div class="product_add_cart">
								<h2 class="c_price"><?php
								if($product->get_sale_price())
									echo get_woocommerce_currency_symbol().$product->get_sale_price() .' - ';
								?>
								<span>
								<?php
								if($product->get_regular_price())
									echo get_woocommerce_currency_symbol().$product->get_regular_price();
								?></h2>
								<h3>Availability: <?php
								if( $product->is_on_sale() ){
									echo '<span>SALE</span>';
								} else {
									echo '<span>'.strtoupper($product->get_stock_status()).'</span>';
								}
								?> </h3>
								<a id="cart_<?php echo $product->get_id(); ?>" onclick="BCOAddToCart(<?php echo $product->get_id(); ?>,<?php echo $product->get_price(); ?>);" href="javascript:void(0);" class="c_add_cart"><i class="fa fa-shopping-basket" aria-hidden="true"></i> Add to cart</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		<?php endwhile;?>
		<?php wp_reset_query(); ?>
	<?php endif;?>


	<?php if( $query->have_posts() ): ?>
	<div class="category_full_list_area" style="display: none;">
		<br>
		<div class="container" style="padding-left: 0; padding-right: 0;">
			<div class="row">
				<?php while( $query->have_posts() ): $query->the_post();?>
					<?php $product = wc_get_product( $query->post->ID );?>
					<div class="col-sm-6 col-md-4">
						<div class="feature_item text-center">
							<div class="feature_item_img">
								<div class="product_cat_img feature_item_img">
									<a href="<?php echo get_the_permalink(); ?>">
										<img style="width: 370px; height: 210px;" width="370" height="210" src="<?php echo !empty(wp_get_attachment_image_src( get_post_thumbnail_id( $loop->post->ID ), 'home-blog-thumb' )[0]) ? wp_get_attachment_image_src( get_post_thumbnail_id( $loop->post->ID ), 'home-blog-thumb' )[0] : wc_placeholder_img_src();?>" alt="<?php echo $product->get_title(); ?>">
									</a>
								</div>
								<?php
								if( $product->is_on_sale() ){
									echo '<span>SALE</span>';
								} else {
									echo '<span>'.strtoupper($product->get_stock_status()).'</span>';
								}
								?>
							</div>
							<a  class="full-list-product-link" href="<?php echo get_the_permalink(); ?>">
								<h2><?php the_title(); ?></h2>
							</a>
							<h3><?php
							if($product->get_regular_price())
								echo get_woocommerce_currency_symbol().$product->get_regular_price();
							?></h3>
							<?php if( (int)$product->get_average_rating() ): ?>
							<a href="javascript:void(0)" class="product_rating">
								<?php for( $i=1; $i <= 5; $i++ ): ?>
									<?php if( $i <= (int)$product->get_average_rating() ):?>
										<i class="fa fa-star-o yellow_star" aria-hidden="true"></i>
									<?php else:?>
										<i class="fa fa-star-o" aria-hidden="true"></i>
									<?php endif;?>
								<?php endfor;?>
							</a>
							<?php else:?>
							<a href="javascript:void(0)" class="product_rating">
								<i class="fa fa-star-o" aria-hidden="true"></i>
								<i class="fa fa-star-o" aria-hidden="true"></i>
								<i class="fa fa-star-o" aria-hidden="true"></i>
								<i class="fa fa-star-o" aria-hidden="true"></i>
								<i class="fa fa-star-o" aria-hidden="true"></i>
							</a>
							<?php endif;?>
						</div>
					</div>
				<?php endwhile;?>
				<?php wp_reset_query(); ?>
			</div>
		</div>
	</div>
	<?php endif;?>

	<div class="pagination_area">
		<div class="container pagination_inner">
			<div class="row">
				<div class="col-md-12">
					<div class="pagination_count">
						<p>Item <?php echo $o_offset; ?> to <?php echo $offset; ?> of <?php echo $total_posts;?> total</p>
					</div>
					<div class="pagination_right">
						<ul class="pagination">
							<?php
								echo paginate_links( array(
									'base'         => add_query_arg('paginate','%#%'),
									'format'       => '?paginate=%#%',
									'total'        => $query->max_num_pages,
									'current'      => isset($_GET['paginate']) ? $_GET['paginate'] : 1,
									'show_all'     => false,
									'type'         => 'plain',
									'end_size'     => 2,
									'mid_size'     => 1,
									'prev_next'    => true,
									'prev_text'    => sprintf( '<i class="fa fa-caret-left" aria-hidden="true"></i>' ),
									'next_text'    => sprintf( '<i class="fa fa-caret-right" aria-hidden="true"></i>' ),
									'add_args'     => false,
									'add_fragment' => '',
								) );
							?>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>

</section>

<script>
function BCOAddToCart(product_id, product_price) {
	var product     = parseInt(product_id);
	var price       = parseFloat(product_price);
	var quantity    = 1;
	var cart_url    = '';
	var total       = null;
	var cart_amount = null;
	var currency_sn = "<?php echo get_woocommerce_currency_symbol(); ?>";

	jQuery('#cart_' + product).html('<i class="fa fa-spin fa-gear" aria-hidden="true"></i> Processing');

	if(product != '') {
		cart_url += "<?php echo home_url();?>/cart/?add-to-cart=" + product;
	}

	if( parseInt(quantity) < 1 ) {
		quantity = 1;
	}

	cart_url += "&quantity=" + quantity;

	jQuery.ajax({url: cart_url, success: function(result) {

		if(typeof price == 'undefined' || price == null || price == '') {
			price = 0;
		}

		if(typeof quantity == 'undefined' || quantity == null || quantity == '') {
			quantity = 0;
		}

		total = parseFloat(price) * parseFloat(quantity);
		cart_amount = jQuery('.woocommerce-Price-amount.amount').text().replace(/[&\/\\#,+()$~%'":*?<>{}]/g, '');

		if(typeof cart_amount == 'undefined' || cart_amount == null || cart_amount == '') {
			cart_amount = 0;
		}

		jQuery('#cart_' + product).html('<i class="fa fa-check" aria-hidden="true"></i> Added');
		jQuery('.cart-total-bco').html( ( parseInt(jQuery('.cart-total-bco').html()) + parseInt(quantity) ) );
		jQuery('.woocommerce-Price-amount.amount').html(  currency_sn + parseFloat( parseFloat(total) + parseFloat(cart_amount) ).toFixed(2) );

	}});

	return;
}

window.onload = function() {
	var category_name = "<?php echo !empty($categories) ? $categories : null;?>";
	if(category_name != '')
		jQuery('div.page_title_area_inner').html('<h2>'+category_name+'</h2>');
}
</script>
